import './App.css';
import { productList } from './helpers/productList'
import List from './components/List';

function App() {
  return (
    <div className="App-header">
      <List productList = {productList} />
    </div>
  );
}

export default App