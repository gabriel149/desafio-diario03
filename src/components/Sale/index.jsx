import "./styles.css";

const Sale = ({ children }) => (
    <strong className = "red"> SALE OFF: {children}%</strong>
);

export default Sale;
