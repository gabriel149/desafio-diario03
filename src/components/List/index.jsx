import Sale from "../Sale";

const List = ({ productList = [] }) => (
  <ul>
    {productList.map(({ name, price, discountPercentage }, index) => (
      <li key={index}>
        {name} - <span> R$ {price} </span>
        {discountPercentage && (
          <Sale>{discountPercentage}</Sale>
        )}
      </li>
    ))}
  </ul>
);

export default List;
